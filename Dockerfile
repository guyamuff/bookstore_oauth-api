# syntax=docker/dockerfile:1

FROM golang:1.18-alpine 
WORKDIR /main
COPY go.mod ./
COPY go.sum ./
RUN go mod download

COPY *.go ./
COPY src/ /main/src/

RUN go build -o /bookstore_oauth-api .
EXPOSE 8080

CMD [ "/bookstore_oauth-api" ]


