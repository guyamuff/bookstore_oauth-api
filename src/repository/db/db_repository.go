package db

import (
	"bookstore_oauth-api/src/clients/cassandra"
	"bookstore_oauth-api/src/domain/access_token"
)

const (
	insertCql = "INSERT INTO access_token (id, userid, clientid, expires) VALUES (?, ?, ?, ?);"
	selectCql = "SELECT id, userid, clientid, expires FROM access_token where id = ?;"
)

type cassandraRepository struct{}

func (r cassandraRepository) Create(at access_token.AccessToken) error {

	if err := cassandra.GetSession().Query(insertCql, at.Id,
		at.UserId, at.ClientId, at.Expires).Exec(); err != nil {
		panic(err)
	}

	return nil
}

func (r cassandraRepository) GetTokById(s string) (*access_token.AccessToken, error) {

	var at access_token.AccessToken

	if err := cassandra.GetSession().Query(selectCql, s).Scan(&at.Id, &at.UserId, &at.ClientId, &at.Expires); err != nil {
		return nil, err
	}

	return &at, nil
}

func NewCassandraRepository() access_token.CassandraRepository {
	return cassandraRepository{}
}
