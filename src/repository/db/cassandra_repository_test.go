package db

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestNewCassandraRepository(t *testing.T) {
	r := NewCassandraRepository()
	assert.NotNil(t, r, "New repository created")
}
