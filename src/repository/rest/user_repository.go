package rest

import (
	"bookstore_oauth-api/src/domain/user"
	"encoding/json"
	goErr "errors"
	"github.com/mercadolibre/golang-restclient/rest"
	"gitlab.com/guyamuff/bookstore_oauth-go/src/errors"
	"time"
)

type UserRepository interface {
	LoginUser(string, string) (*user.User, error)
}

type userRepository struct {
}

var (
	restClient = rest.RequestBuilder{
		BaseURL: "http://localhost:8080",
		Timeout: 100 * time.Millisecond,
	}
)

func NewRestRepository() UserRepository {
	return &userRepository{}
}

func (r *userRepository) LoginUser(email string, password string) (*user.User, error) {
	request := user.UserLoginRequest{
		Email:    email,
		Password: password,
	}

	/* The rest client suggested by the course does not support mocking for go versions greater than 1.12. The
	project has not been updated in 5 years. I would rather learn any particulars about recent go versions and don't
	care about the particulars of an abandoned library, so I do not want to downgrade my go version. Web search doesn't
	suggest any alternative libraries. Implementing with go net/http will work, but want to get back to the course and
	not to start down the rabbit hole of making a mock-friendly rest client.
	*/

	resp := restClient.Post("/users/login", request)
	if resp == nil || resp.Response == nil {
		return nil, goErr.New("bad rest response from user api")
	}

	if resp.StatusCode > 299 {
		var restErr errors.RestErr
		err := json.Unmarshal(resp.Bytes(), &restErr)
		if err == nil {
			return nil, goErr.New(restErr.Message)
		} else {
			return nil, err
		}
	}

	var u user.User
	err := json.Unmarshal(resp.Bytes(), &u)
	return &u, err
}
