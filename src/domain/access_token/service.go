package access_token

import (
	"math/rand"
	"strconv"
)

type Service interface {
	Create(request AccessTokenRequest) (*AccessToken, error)
	GetTokenById(string) (*AccessToken, error)
}

type service struct {
	db      CassandraRepository
	userRep RestUserRepository
}

func NewService(db CassandraRepository, userRep RestUserRepository) Service {
	return &service{db: db, userRep: userRep}
}

func (s *service) Create(at AccessTokenRequest) (*AccessToken, error) {
	user, err := s.userRep.LoginUser(at.Username, at.Password)
	if err != nil {
		return nil, err
	}

	var token AccessToken
	token = NewAccessToken()
	token.Id = strconv.Itoa(rand.Int()) // Course is vague on some of these details
	token.UserId = user.Id

	dbErr := s.db.Create(token)
	if dbErr != nil {
		return nil, dbErr
	}
	return &token, nil
}

func (s *service) GetTokenById(id string) (*AccessToken, error) {
	return s.db.GetTokById(id)
}
