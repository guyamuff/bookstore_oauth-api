package access_token

import (
	"bookstore_oauth-api/src/domain/user"
	"errors"
	"strings"
	"time"
)

const expirationTime = 24

type AccessToken struct {
	Id       string `json:"id"`
	UserId   int64  `json:"userId"`
	ClientId int64  `json:"clientId"`
	Expires  int64  `json:"expires"`
}

type AccessTokenRequest struct {
	GrantType string `json:"grant_type"`
	Username  string `json:"username"`
	Password  string `json:"password"`
}

func NewAccessToken() AccessToken {
	return AccessToken{
		Expires: time.Now().UTC().Add(expirationTime * time.Hour).Unix(),
	}
}

func (at AccessToken) IsExpired() bool {
	return time.Unix(at.Expires, 0).Before(time.Now().UTC())
}

func (at AccessToken) Validate() error {
	at.Id = strings.TrimSpace(at.Id)
	if at.Id == "" {
		return errors.New("id must not be blank")
	}

	if at.UserId <= 0 {
		return errors.New("userid must be a positive integer")
	}

	if at.ClientId <= 0 {
		return errors.New("clientid must be a positive integer")
	}

	if at.Expires <= 0 {
		return errors.New("expires must be a positive integer")
	}
	return nil
}

func (at AccessTokenRequest) Validate() error {
	switch at.GrantType {
	case "password":
		return nil
	default:
		return errors.New("Unsupported grant type: " + at.GrantType)
	}
}

type CassandraRepository interface {
	Create(at AccessToken) error
	GetTokById(string) (*AccessToken, error)
}

type RestUserRepository interface {
	LoginUser(string, string) (*user.User, error)
}
