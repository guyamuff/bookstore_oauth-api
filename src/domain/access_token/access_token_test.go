package access_token

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestNewAccessToken(t *testing.T) {
	token := NewAccessToken()
	assert.False(t, token.IsExpired(), "New token should not be expired")
	assert.EqualValues(t, "", token.Id, "Brand new token id unset")
	assert.EqualValues(t, 0, token.UserId, "Brand new token user not yet set")
	assert.EqualValues(t, 0, token.ClientId, "Brand new token client not yet set")
}

func TestNewService(t *testing.T) {
	s := NewService(nil, nil)
	assert.NotNil(t, s, "service created")
	assert.Panics(t, func() {
		_, _ = s.GetTokenById("blah")
	}, "Not implemented")
}
