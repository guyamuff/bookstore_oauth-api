package app

import (
	"bookstore_oauth-api/src/domain/access_token"
	"bookstore_oauth-api/src/http"
	"bookstore_oauth-api/src/repository/db"
	"bookstore_oauth-api/src/repository/rest"
	"fmt"
	"github.com/gin-gonic/gin"
)

var (
	router = gin.Default()
)

func StartApplication() {
	err := router.SetTrustedProxies(nil)
	if err != nil {
		fmt.Print(err.Error())
		return
	}

	cr := db.NewCassandraRepository()
	rr := rest.NewRestRepository()
	serv := access_token.NewService(cr, rr)
	ath := http.NewAccessTokenHandler(serv)
	router.GET("access_token/:token", ath.GetById)
	router.POST("access_token", ath.Create)

	err = router.Run(":8081")
	if err != nil {
		fmt.Print(err.Error())
		return
	}
}
