package http

import (
	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestNewAccessTokenHandler(t *testing.T) {

	h := NewAccessTokenHandler(nil)
	assert.NotNil(t, h, "Handler created")

	ctx := gin.Context{}
	ctx.AddParam(TokenParam, "123")
	assert.Panics(t, func() {
		h.GetById(&ctx)
	}, "Not implemented")
}
