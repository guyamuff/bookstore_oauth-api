package http

import (
	"bookstore_oauth-api/src/domain/access_token"
	"github.com/gin-gonic/gin"
	"gitlab.com/guyamuff/bookstore_oauth-go/src/errors"
	"net/http"
)

const TokenParam = "token"

type AccessTokenHandler interface {
	Create(*gin.Context)
	GetById(*gin.Context)
}

type accessTokenHandler struct {
	s access_token.Service
}

func (a accessTokenHandler) Create(ctx *gin.Context) {
	var at access_token.AccessTokenRequest
	err := ctx.ShouldBindJSON(&at)
	if err != nil {
		httpErr := errors.NewBadRequestError("Malformed JSON: " + err.Error())
		ctx.JSON(httpErr.Status, httpErr)
		return
	}

	err = at.Validate()
	if err != nil {
		httpErr := errors.NewBadRequestError(err.Error())
		ctx.JSON(httpErr.Status, httpErr)
		return
	}

	token, err := a.s.Create(at)
	if err != nil {
		httpErr := errors.NewInternalError(err.Error())
		ctx.JSON(httpErr.Status, httpErr)
		return
	}

	ctx.JSON(http.StatusCreated, token)
}

func (a accessTokenHandler) GetById(ctx *gin.Context) {
	t := ctx.Param(TokenParam)
	if len(t) == 0 {
		httpErr := errors.NewBadRequestError("Must specify token id")
		ctx.JSON(httpErr.Status, httpErr)
		return
	}

	at, e := a.s.GetTokenById(t)
	if e != nil {
		httpErr := errors.NewInternalError(e.Error())
		ctx.JSON(httpErr.Status, httpErr)
		return
	}
	ctx.JSON(http.StatusOK, at)
}

func NewAccessTokenHandler(service access_token.Service) AccessTokenHandler {
	return &accessTokenHandler{s: service}
}
