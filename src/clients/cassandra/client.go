package cassandra

import (
	"github.com/gocql/gocql"
)

const keyspace = "oauth"

var (
	cluster *gocql.ClusterConfig
	session *gocql.Session
)

func init() {
	cluster = gocql.NewCluster("127.0.0.1")
	cluster.Keyspace = keyspace
	cluster.Consistency = gocql.Quorum
}

func GetSession() *gocql.Session {
	if session == nil {
		var err error
		session, err = cluster.CreateSession()
		if err != nil {
			panic(err)
		}
	}
	return session
}

// create keyspace oauth with replication = {'class':'SimpleStrategy', 'replication_factor':1};
// create table oauth.access_token(id varchar primary key, userid bigint, clientid bigint, expires bigint);
